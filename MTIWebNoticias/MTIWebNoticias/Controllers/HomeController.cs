﻿using MTIWebNoticias.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace MTIWebNoticias.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Método que retorna una lista TOP 4 del modelo de noticias
        /// </summary>
        /// <returns>View(NoticiasModel)</returns>
        public ActionResult Index()
        {
            return View(GetPortadaActual().Take(4));
        }

        /// <summary>
        /// Método que retorna una lista del modelo de noticias de tipo "World"
        /// </summary>
        /// <returns>View(NoticiasModel)</returns>
        public ActionResult World()
        {
            var noticia = GetPortadaActual().Where(m => m.ControladorWeb == "World");
            return View(noticia);
        }

        /// <summary>
        /// Método que retorna una lista del modelo de noticias de tipo "Technology" 
        /// </summary>
        /// <returns>View(NoticiasModel)</returns>
        public ActionResult Technology()
        {
            var noticia = GetPortadaActual().Where(m => m.ControladorWeb == "Technology");
            return View(noticia);
        }

        /// <summary>
        /// Método que retorna una lista del modelo de noticias de tipo "Business"
        /// </summary>
        /// <returns>View(NoticiasModel)</returns>
        public ActionResult Business()
        {
            var noticia = GetPortadaActual().Where(m => m.ControladorWeb == "Business");
            return View(noticia);
        }

        /// <summary>
        /// Método que retorna una lista del modelo de noticias de tipo "Politics"
        /// </summary>
        /// <returns>View(NoticiasModel)</returns>
        public ActionResult Politics()
        {
            var noticia = GetPortadaActual().Where(m => m.ControladorWeb == "Politics");
            return View(noticia);
        }

        /// <summary>
        /// Método que retorna la lista completa del modelo de noticias
        /// </summary>
        /// <returns>View(NoticiasModel)</returns>
        public PartialViewResult Topbar()
        {
            return PartialView("_Topbar", GetPortadaActual());
        }

        /// <summary>
        /// Método que retorna la lista completa del modelo de noticias
        /// Desde un archivo XML alojado en Azure Blob Storage
        /// </summary>
        /// <returns>IEnumerable<NoticiaModel></returns>
        private IEnumerable<NoticiaModel> GetPortadaActual()
        {
            XDocument noticiaXML = XDocument.Load(ConfigurationManager.AppSettings["XmlDocumentNoticias"]);
            XNamespace ns = ConfigurationManager.AppSettings["XmlNamespaceNoticias"];
            var noticias = from noticia in noticiaXML.Descendants(ns + "Item")
                           select new NoticiaModel()
                           {
                               Autor = noticia.Element(ns + "Autor").Value,
                               Categoria = noticia.Element(ns + "Categoria").Value,
                               Ciudad = noticia.Element(ns + "Ciudad").Value,
                               ControladorWeb = noticia.Element(ns + "ControladorWeb").Value,
                               Cuerpo = noticia.Element(ns + "Cuerpo").Value,
                               FechaPublicacion = noticia.Element(ns + "FechaPublicacion").Value,
                               ImagenAutor = noticia.Element(ns + "ImagenAutor").Value,
                               ImagenPortada = noticia.Element(ns + "ImagenPortada").Value,
                               Pais = noticia.Element(ns + "Pais").Value,
                               Resumen = noticia.Element(ns + "Resumen").Value,
                               Titulo = noticia.Element(ns + "Titulo").Value
                           };
            
            return noticias;
        }
    }
}