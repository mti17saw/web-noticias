﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTIWebNoticias.Models
{
    public class NoticiaModel
    {
        public string Autor { get; set; }
        public string Categoria { get; set; }
        public string Ciudad { get; set; }
        public string ControladorWeb { get; set; }
        public string Cuerpo { get; set; }
        public string FechaPublicacion { get; set; }
        public string ImagenAutor { get; set; }
        public string ImagenPortada { get; set; }
        public string Pais { get; set; }
        public string Resumen { get; set; }
        public string Titulo { get; set; }
    }
}